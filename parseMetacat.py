import os
import sys
import csv
from xml.dom import minidom

with open('nanorp.csv', 'w') as csvfile:
    fieldnames = ['docId', 'dataId', 'title']
    writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

    writer.writeheader()

    if len(sys.argv) > 1:
        base_dir = sys.argv[1]
        entries = os.listdir(base_dir)

        for entry in entries:
            try:
                file_path = os.path.join(base_dir, entry)
                doc = minidom.parse(file_path)
                eml = doc.getElementsByTagName('eml:eml')
                docId = eml[0].attributes['packageId'].value
                #print('========')
                #print(docId)
                data_tables = doc.getElementsByTagName('dataTable')
                for data_table in data_tables:
                    entityName = data_table.getElementsByTagName('entityName')
                    dataId = entityName[0].firstChild.data
                    #print(dataId)
                    distribution = data_table.getElementsByTagName('distribution')
                    for d in distribution:
                        online = d.getElementsByTagName('online')
                        for o in online:
                            url = o.getElementsByTagName('url')
                            title = url[0].firstChild.data
                            #print(title)
                            writer.writerow({'docId': docId, 'dataId': dataId, 'title': title})
            except Exception:
                print(Exception.message)

    else:
        print('Add a base directory example: python parseMetacat.py </path/to/directory>')
